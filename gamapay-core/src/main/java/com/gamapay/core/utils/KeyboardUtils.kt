package com.gamapay.core.utils

import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment


/**
 * Created by AlexYang on 2023/9/6.
 *
 * KotlinExtension with Keyboard
 */
val Activity.hideSoftKeyboard: Unit
    get() {
        currentFocus?.let {
            (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(it.windowToken, 0)
        }
    }

val Fragment.hideKeyboardInFragment: Unit
    get() {
        activity?.let { act ->
            (act.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).let { imm ->
                this@hideKeyboardInFragment.view?.rootView?.windowToken?.let { token ->
                    imm.hideSoftInputFromWindow(token, InputMethodManager.RESULT_UNCHANGED_SHOWN)
                }
            }
        }
    }

val Activity.softInputAdjustPan: Unit
    get() = window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

val Fragment.softInputAdjustPan: Unit
    get() = requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

/**
 * 取得View焦點並開啟鍵盤
 */
val View.showKeyboard: Unit
    get() {
        (context.applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)?.let { imm ->
            requestFocus()
            imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
        }
    }

/**
 * 取得View焦點並開啟鍵盤
 */
val View.showKeyboardAndFocus: Unit
    get() {
        (context.applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)?.let { imm ->
            requestFocus()
            imm.toggleSoftInput(
                InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY
            )
        }
    }

/**
 * 延遲300ms取得View焦點並開啟鍵盤
 */
val View.showKeyboardDelay: Unit
    get() {
        (context.applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)?.let { imm ->
            postDelayed({
                requestFocus()
                imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
            }, 300)
        }
    }

val View.hideKeyboard: Unit
    get() {
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(windowToken, 0)
    }

/**
 * 檢查檢盤是否顯示
 */
val View.isKeyboardShown: Boolean
    get(): Boolean {
        with(Rect()) {
            rootView.getWindowVisibleDisplayFrame(this)
            return rootView.bottom - bottom > 100 * rootView.resources.displayMetrics.density
        }
    }