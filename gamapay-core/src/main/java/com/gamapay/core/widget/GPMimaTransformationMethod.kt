@file:Suppress("SpellCheckingInspection")

package com.gamapay.core.widget

import android.graphics.Rect
import android.text.method.TransformationMethod
import android.view.View

/**
 * Created by AlexYang on 2023/9/4.
 *
 * 客製化 EditText 隱碼樣式
 */
class GPMimaTransformationMethod(private val customChar: Char) : TransformationMethod {

    override fun getTransformation(source: CharSequence?, view: View?): CharSequence {
        return MimaCharSequence(source)
    }

    override fun onFocusChanged(
        view: View?,
        sourceText: CharSequence?,
        focused: Boolean,
        direction: Int,
        previouslyFocusedRect: Rect?
    ) {
    }

    inner class MimaCharSequence(private val source: CharSequence?) : CharSequence {
        override val length: Int
            get() = source?.length ?: 0

        override fun get(index: Int) = customChar

        override fun subSequence(startIndex: Int, endIndex: Int) =
            MimaCharSequence(source?.subSequence(startIndex, endIndex))
    }
}