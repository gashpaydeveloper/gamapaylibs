package com.gamapay.core.widget

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.text.TextPaint
import android.text.style.ReplacementSpan

/**
 * Created by AlexYang on 2023/9/15.
 *
 * Tag 類型的文字 Span
 */
class GPTextTagSpan(
    private val fontSizePx: Int = 0,
    private val backgroundColor: Int = Color.TRANSPARENT
) : ReplacementSpan() {

    override fun getSize(
        paint: Paint,
        text: CharSequence,
        start: Int,
        end: Int,
        fm: Paint.FontMetricsInt?
    ) = gpTextPaint(paint).measureText(text.subSequence(start, end).toString()).toInt()


    override fun draw(
        canvas: Canvas,
        text: CharSequence,
        start: Int,
        end: Int,
        x: Float,
        top: Int,
        y: Int,
        bottom: Int,
        paint: Paint
    ) {
        val str = text.subSequence(start, end)
        val p = gpTextPaint(paint)
        val fm = p.fontMetrics
        val textWidth = p.measureText(str.toString())

        /** Draw background rounded rectangle **/
        /** 預設圓角約 4dp **/
        canvas.drawRoundRect(
            RectF(x, top.toFloat(), x + textWidth, bottom.toFloat()),
            12f,
            12f,
            Paint().apply { color = backgroundColor }
        )

        /** Calculate vertical centering **/
        val centerY = y - ((y + fm.descent + y + fm.ascent) / 2 - (bottom + top) / 2)

        /** Draw text **/
        canvas.drawText(str.toString(), x, centerY, p)
    }

    private fun gpTextPaint(srcPaint: Paint) =
        TextPaint(srcPaint).apply { textSize = fontSizePx.toFloat() }
}