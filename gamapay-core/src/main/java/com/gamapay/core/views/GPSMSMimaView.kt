@file:Suppress("SpellCheckingInspection")

package com.gamapay.core.views

import android.content.Context
import android.graphics.Typeface
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.Gravity
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import com.gamapay.core.R
import com.gamapay.core.databinding.ViewGpSmsMimaBinding
import com.gamapay.core.utils.hideKeyboard
import com.gamapay.core.utils.showKeyboard

/**
 * Created by AlexYang on 2023/9/6.
 *
 * SMS驗證碼View
 *
 * ref：PasswordView
 * 將原本放EditText的LinearLayout，換成ConstraintLayout
 * 原因為讓EditText的寬高比為1:1
 */
class GPSMSMimaView(context: Context, private val attrs: AttributeSet?) : LinearLayout(context, attrs),
    View.OnFocusChangeListener, View.OnKeyListener, TextWatcher {
    private val binding by lazy { DataBindingUtil.bind<ViewGpSmsMimaBinding>(LayoutInflater.from(context).inflate(R.layout.view_gp_sms_mima, this).findViewById(R.id.containerView)) }
    private val inputList by lazy { mutableListOf<EditText>() }
    private val model by lazy { GPMimaViewViewModel() }
    private lateinit var onInputListener: (mima: String) -> Unit
    private lateinit var onFullListener: (mima: String) -> Unit
    /**
     * 設定或取得輸入框密碼
     */
    var mimia: String
        get() = binding?.etMima?.text.toString()
        set(value) {
            binding?.etMima?.setText(value)
        }

    /**
     * 取得密碼總長度
     */
    val mimaLength = model.passwordLength

    init {
        initData
        initViews
    }

    /**
     * 初始化Style Data
     */
    private val initData: Unit
        get() {
            with(model) {
                val typedArray = context.obtainStyledAttributes(attrs, R.styleable.GPSMSMimaView)
                (0 until typedArray.indexCount).forEach {
                    when (val attr = typedArray.getIndex(it)) {
                        R.styleable.GPSMSMimaView_isShowText -> showText = typedArray.getBoolean(attr, showText)
                        R.styleable.GPSMSMimaView_passwordLength -> passwordLength = typedArray.getInteger(attr, passwordLength)
                        R.styleable.GPSMSMimaView_minGap -> minGap = typedArray.getDimension(attr, minGap)
                        R.styleable.GPSMSMimaView_itemWidth -> itemWidth = typedArray.getDimensionPixelOffset(attr, itemWidth)
                        R.styleable.GPSMSMimaView_itemBgColor -> itemBgColor = typedArray.getColor(attr, itemBgColor)
                        R.styleable.GPSMSMimaView_itemTextSize -> itemTextSize = typedArray.getDimensionPixelSize(attr, itemTextSize)
                        R.styleable.GPSMSMimaView_itemBg -> itemBg = typedArray.getResourceId(attr, itemBg)
                        R.styleable.GPSMSMimaView_itemErrorBg -> itemErrorBg = typedArray.getResourceId(attr, itemErrorBg)
                        R.styleable.GPSMSMimaView_itemTvColor -> itemTextColor = typedArray.getColor(attr, itemTextColor)
                    }
                }
                typedArray.recycle()
            }
        }

    private val initViews: Unit
        get() {
            binding?.run {
                val constraintLayout = clMimaContainer
                with(model) {
                    for (i in 0 until passwordLength) {
                        val etView = EditText(context)
                        etView.id = View.generateViewId()
                        val layoutParams = ConstraintLayout.LayoutParams(0, LayoutParams.MATCH_PARENT)
                        if (i != 0) layoutParams.marginStart = minGap.toInt()
                        etView.layoutParams = layoutParams
                        etView.gravity = Gravity.CENTER
                        etView.setTypeface(etView.typeface, Typeface.BOLD)
                        /** 設定文字大小 **/
                        etView.textSize = itemTextSize.toFloat()
                        /** 設定文字顏色 **/
                        etView.setTextColor(itemTextColor)
                        etView.inputType = InputType.TYPE_CLASS_NUMBER
                        etView.isLongClickable = false
                        etView.onFocusChangeListener = this@GPSMSMimaView
                        etView.setBackgroundResource(itemBg)
                        etView.setOnKeyListener(this@GPSMSMimaView)
                        constraintLayout.addView(etView)
                        inputList.add(etView)
                    }

                    val constraintSet = ConstraintSet()
                    constraintSet.clone(constraintLayout)
                    for (i in 0 until passwordLength) {
                        /** 設定水平鏈結_view_start **/
                        if (i == 0) {
                            constraintSet.connect(inputList[i].id, ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START)
                        } else {
                            constraintSet.connect(inputList[i].id, ConstraintSet.START, inputList[i - 1].id, ConstraintSet.END,)
                        }
                        /** 設定水平鏈結_view_end **/
                        if (i == inputList.count() - 1) {
                            constraintSet.connect(inputList[i].id, ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END)
                        } else {
                            constraintSet.connect(inputList[i].id, ConstraintSet.END, inputList[i + 1].id, ConstraintSet.START)
                        }

                        /** 設定寬高比為1:1 **/
                        constraintSet.constrainHeight(inputList[i].id, 0)
                        constraintSet.constrainWidth(inputList[i].id, 0)
                        constraintSet.setDimensionRatio(inputList[i].id, "1:1")
                    }
                    constraintSet.applyTo(constraintLayout)

                    with(etMima) {
                        addTextChangedListener(this@GPSMSMimaView)
                        setOnKeyListener(this@GPSMSMimaView)
                        postDelayed({ requestFocus }, 300)
                    }
                }
            }
        }

    /**
     * 轉移顯示輸入框focus給隱藏輸入框
     */
    override fun onFocusChange(v: View, hasFocus: Boolean) {
        takeIf { v.hasFocus() }?.run {
            binding?.run {
                with(etMima) {
                    isFocusable = true
                    isFocusableInTouchMode = true
                    requestFocus()
                    showKeyboard
                }
            }
        }
    }

    /**
     * 處理鍵盤刪除字元事件
     */
    override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
        takeIf { event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL }?.run {
            binding?.run {
                takeIf { etMima.length() > 0 }?.run {
                    inputList[etMima.text.length - 1].setText("")
                    inputList[etMima.text.length - 1].setBackgroundResource(model.itemBg)
                    etMima.setText(etMima.text.subSequence(0, etMima.length() - 1))
                    etMima.post { etMima.setSelection(etMima.text.toString().length) }
                } ?: etMima.setText("")
                return true
            }
        }
        return false
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) = setData(s.length)

    override fun afterTextChanged(s: Editable?) {}

    /**
     * 清空輸入框密碼
     *
     */
    val clearMima: Unit
        get() {
            binding?.etMima?.run {
                postDelayed({
                    inputList.forEach { it.setText("") }
                    setText("")
                    requestFocus
                }, 300)
            }
        }

    val clearMimaNonFocus: Unit
        get() {
            binding?.etMima?.run {
                postDelayed({
                    inputList.forEach { it.setText("") }
                    setText("")
                }, 300)
            }
        }

    /**
     * 設定輸入密碼邏輯
     */
    private fun setData(inputLen: Int) {
        takeIf { inputLen == 0 }?.run { return@setData }
        takeIf { ::onInputListener.isInitialized }?.run { onInputListener(this@GPSMSMimaView.mimia) }
        takeIf { inputLen == model.passwordLength && ::onFullListener.isInitialized }?.run {
            onFullListener(this@GPSMSMimaView.mimia)
            binding?.etMima?.hideKeyboard
        }
        (0 until inputLen).forEach {
            inputList[it].setText(takeIf { model.showText }?.run { binding?.etMima?.text.toString()[it].toString() } ?: "\u2022")
            setDefaultBg(inputList[it])
        }
        (inputLen until model.passwordLength).forEach { inputList[it].setText("") }
    }

    /**
     * 設定未輸入框背景
     */
    private fun setDefaultBg(text: EditText) = text.setBackgroundResource(model.itemBg)

    /**
     * 設定錯誤輸入框背景
     */
    private fun setErrorBg(text: EditText) = text.setBackgroundResource(model.itemErrorBg)

    /**
     * 輸入框取得焦點
     */
    private val requestFocus: Unit
        get() {
            binding?.etMima?.run {
                isFocusable = true
                isFocusableInTouchMode = true
                requestFocus()
                showKeyboard
            }
        }

    //====== public method  =========================================================================

    /**
     * 設定輸入框輸入時觸發事件
     */
    fun setOnInputMima(onInputListener: (mima: String) -> Unit) {
        this.onInputListener = onInputListener
    }

    /**
     * 設定輸入框全輸入時觸發事件
     */
    fun setOnFullPassword(onFullListener: (mima: String) -> Unit) {
        this.onFullListener = onFullListener
    }

    /**
     * 將所有欄位的背境，設定為預設背景
     */
    fun setAllBackgroundDefault() {
        (0 until model.passwordLength).forEach { setDefaultBg(inputList[it]) }
    }

    /**
     * 將所有欄位的背境，設定為錯誤背景
     */
    fun setAllBackgroundError() {
        (0 until model.passwordLength).forEach { setErrorBg(inputList[it]) }
    }

    /**
     * 將所有隱碼密碼輸入框，開啟可輸入模式
     */
    fun enableInputMima() = binding?.run {
        clMimaContainer.children.filterIsInstance<EditText>().take(model.passwordLength)
            .forEach { view ->
                view.isCursorVisible = true
                view.isFocusable = true
                view.isFocusableInTouchMode = true
            }
    }

    /**
     * 將所有隱碼密碼輸入框，禁止輸入
     */
    fun disableInputMima() = binding?.run {
        clMimaContainer.children.filterIsInstance<EditText>().take(model.passwordLength)
            .forEach { view ->
                view.isCursorVisible = false
                view.isFocusable = false
            }
    }
}

data class GPMimaViewViewModel(
    var showText: Boolean = false,
    var passwordLength: Int = 0,
    var minGap: Float = 10f,
    var itemWidth: Int = 50,
    var itemBgColor: Int = -1,
    var itemTextSize: Int = -1,
    var inputType: Int = 0,
    var password: Boolean = false,
    var itemBg: Int = R.drawable.bg_mima_view,
    var itemErrorBg: Int = R.drawable.bg_mima_view_error,
    var itemTextColor: Int = -1
)
