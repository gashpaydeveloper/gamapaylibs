package com.gamapay.core.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import com.gamapay.core.R

/**
 * Created by AlexYang on 2023/9/4.
 *
 * 垂直虛線View
 */
class GPVerticalDashLineView(context: Context, attrs: AttributeSet? = null) : View(context, attrs) {
    private val paint = Paint()
    private var gpWidth = 4f
    private var gpDashWidth = 4f
    private var gpDashGap = 2f
    private var dashColor = Color.RED

    init {
        paint.color = Color.RED
        paint.strokeWidth = 1f
        paint.pathEffect = DashPathEffect(floatArrayOf(8f, 4f), 0f)
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.GPVerticalDashLineView)
        gpWidth = typedArray.getDimension(R.styleable.GPVerticalDashLineView_gp_width, gpWidth)
        gpDashWidth = typedArray.getDimension(R.styleable.GPVerticalDashLineView_gp_dash_width, gpDashWidth)
        gpDashGap = typedArray.getDimension(R.styleable.GPVerticalDashLineView_gp_dash_gap, gpDashGap)
        dashColor = typedArray.getColor(R.styleable.GPVerticalDashLineView_gp_dash_color, dashColor)
        typedArray.recycle()

        paint.color = dashColor
        paint.strokeWidth = gpWidth
        paint.pathEffect = DashPathEffect(floatArrayOf(gpDashWidth, gpDashGap), 0f)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.drawLine(
            width / 2f,
            0f,
            width / 2f,
            height.toFloat(),
            paint
        )
    }

    /** 設定虛線的寬度 **/
    fun setDashWidth(width: Float) {
        gpWidth = width
        paint.strokeWidth = gpWidth
        invalidate()
    }

    /** 設定虛線的顏色 **/
    fun setDashColor(color: Int) {
        dashColor = color
        paint.color = dashColor
        invalidate()
    }
}