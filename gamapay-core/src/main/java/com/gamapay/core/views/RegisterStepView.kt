package com.gamapay.core.views

import android.animation.ValueAnimator
import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.animation.DecelerateInterpolator
import android.view.animation.LinearInterpolator
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import com.gamapay.core.R
import com.gamapay.core.databinding.ViewRegisterStepBinding
import com.google.android.material.progressindicator.CircularProgressIndicator

/**
 * Created by jackyang on 2023/9/6.
 * 客制註冊階段View
 */
class RegisterStepView @JvmOverloads constructor(
    ctx: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(ctx, attrs, defStyleAttr) {

    private val attributes: TypedArray = ctx.obtainStyledAttributes(attrs, R.styleable.RegisterStepView)
    private val binding by lazy { DataBindingUtil.inflate<ViewRegisterStepBinding>(LayoutInflater.from(ctx), R.layout.view_register_step, null, false) }

    init {
        addView(binding.root)
        /** 設定註冊階段文字 **/
        binding.tvStep.text = attributes.getString(R.styleable.RegisterStepView_stepText)
        /** 設定註冊階段動畫 **/
        binding.cpbStep.circularProgressIndicatorAnimation(attributes.getInteger(R.styleable.RegisterStepView_initialProgress, 0), attributes.getInteger(R.styleable.RegisterStepView_targetProgress, 0))
    }
}

/**
 * 顯示Circular進度條動畫
 * @param initialProgress 初始進度
 * @param targetProgress 結束進度
 */
@BindingAdapter(value = ["initialProgress", "targetProgress"], requireAll = true)
inline fun CircularProgressIndicator.circularProgressIndicatorAnimation(initialProgress: Int, targetProgress: Int) {
    ValueAnimator.ofInt(initialProgress, targetProgress).apply {
        interpolator = LinearInterpolator()
        duration = 1000L
        addUpdateListener { valueAnimator -> this@circularProgressIndicatorAnimation.progress = valueAnimator.animatedValue as Int }
        start()
    }
}