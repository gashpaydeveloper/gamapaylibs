package com.gashpay.personal.gamapaylibs

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.gamapay.core.views.GPSMSMimaView
import com.google.android.material.button.MaterialButton

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val gpMimaView = findViewById<GPSMSMimaView>(R.id.gp_mima)
        gpMimaView.setOnFullPassword {
            Log.i("TAG", "輸入的密碼：${it}")
        }
    }
}